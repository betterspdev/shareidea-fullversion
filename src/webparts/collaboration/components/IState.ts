export interface IState{
    collabID: number;
    settingSeemore: string;
    createdDate: any;
    modifiedDate: any;

    onSearching: boolean;
    searchTxt: string;
    searchSelectScroll: boolean;
    glowingPost: boolean;

    currentState: string;
    allComments: any[];
    enablePing: boolean;
    commentText: boolean;
    currentTab: string;
    currentUserInfo: any;
    scroll: number;
    scrollH: number;
    scrollCapture:boolean;
    startCapture:number;
    firstRender: boolean;
    changeScroll: boolean;
    parentPost: string;

    toggleReply: boolean;

    suggestBlockPosition: any;
    suggestBlockID: string;
    suggestSearch: string;
    filterSuggest: any[];
    suggestSelect: any[];

    userActionPermission: string;
    userGroup: string[];

    shareWith: string[];
    currentPin: number;

    toggleShareID: string;
    shareSelect: string[];
    shareSelectID: number;

    allTasks: any[];

    attachmentSelect: any;
    fileFolder: string;

    //Flag
    toggleFlagID: string;
    flagKeySelected: string;

    //Widget
    currentWidget: string;

    //Assign Task widget
    selectStatus: any;
    dataTask: any;

    selected_lane: string;

    newPostComment: any[];

    alertStatus: string;

    toggleMobileNav: boolean;

    pageMessage: number;
    pageFile: number;

    newCommentChange: boolean;

    currentTablePage: number;
    searchTable: string;
    sortTable: string;

    tooltipMobile: string;
    onLoading: string;
    onSendRequest: any[];
    onReplyLoading: any[];
    displayAlert: string;
    newTextAlert: string;

    parentTopic: string,
    parentItem: any,
    parentUser: any
}