declare interface IPostContentWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'PostContentWebPartStrings' {
  const strings: IPostContentWebPartStrings;
  export = strings;
}
