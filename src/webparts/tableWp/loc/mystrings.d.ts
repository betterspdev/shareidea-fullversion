declare interface ITableWpWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TableWpWebPartStrings' {
  const strings: ITableWpWebPartStrings;
  export = strings;
}
